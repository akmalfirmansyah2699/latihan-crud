<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class BukuController extends Controller
{
    public function master(){
        return view('layouts.master');
    }
    public function erd(){
        return view('buku.erd');
    }
    public function create(){
        return view('buku.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            "judul"=>'required|unique:buku',
            "jumlah_halaman"=>'required',
            "tahun_terbit"=>'required',
            "description"=>'required'
        ]);

        $query= DB::table('buku')->insert([
            "judul" => $request['judul'],
            "jumlah_halaman"=> $request['jumlah_halaman'],
            "tahun_terbit"=> $request['tahun_terbit'],
            "description"=> $request['description']
        ]);

        return redirect('/buku')->with('success','Buku berhasil disimpan!');
    }

    public function index(){
        $buku= DB::table('buku')->get();
        // dd($buku);
        return view('buku.index', compact('buku'));
    }

    public function show($buku_id){
        $buku=DB::table('buku')->where('id', $buku_id)->first();
        // dd($post);
        return view('buku.show', compact('buku'));

    }

    public function edit($buku_id){
        $buku=DB::table('buku')->where('id', $buku_id)->first();
        // dd($post);
        return view('buku.edit', compact('buku'));
    
    }

    public function update(Request $request){
        // dd($request->all());
        $request->validate([
            "judul"=>'required|unique:buku',
            "jumlah_halaman"=>'required',
            "tahun_terbit"=>'required',
            "description"=>'required'
        ]);

        $query= DB::table('buku')->update([
            "judul" => $request['judul'],
            "jumlah_halaman"=> $request['jumlah_halaman'],
            "tahun_terbit"=> $request['tahun_terbit'],
            "description"=> $request['description']
        ]);

        return redirect('/buku')->with('success','Buku berhasil diperbarui!');
    }

    public function destroy($buku_id){
        $query= DB::table('buku')->where('id', $buku_id)->delete();

        return redirect('/buku')->with('success','Buku Berhasil dihapus!');
    }
}
