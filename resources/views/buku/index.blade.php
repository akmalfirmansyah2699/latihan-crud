@extends('layouts.master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">List Buku</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
              @endif
              <a class="btn btn-primary mb-3" href="/buku/create">Buat Buku</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>   
                      <th style="width: 10px">No</th>
                      <th>Judul</th>
                      <th style="width: 10px">Jumlah Halaman</th>
                      <th style="width: 10px">Tahun Terbit</th>
                      <th>Description</th>
                      <th style="width: 10px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($buku as $key => $buku)
                    <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$buku->judul}}</td>
                      <td>{{$buku->jumlah_halaman}}</td>
                      <td>{{$buku->tahun_terbit}}</td>
                      <td>{{$buku->description}}</td>
                      <td style="display:flex;">
                        <a class="btn btn-info btn-sm" href="/buku/{{$buku->id}}">Show</a>
                        <a class="btn btn-primary btn-sm ml-2" href="/buku/{{$buku->id}}/edit">Edit</a>
                        <form action="/buku/{{$buku->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2">
                        </form>
                      </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" align="center">No Posts!</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
@endsection

@push('scripts')
    <script>
        Swal.fire({
            title: "Berhasil!",
            text: "Memasangkan script sweet alert",
            icon: "success",
            confirmButtonText: "Cool",
        });
    </script>

@endpush