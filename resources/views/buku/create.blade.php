@extends('layouts.master')
@section('content')
    <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Membuat Buku</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/buku" method="POST">
                    @csrf
                    <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Masukan Judul">
                        @error('judul')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_halaman">Jumlah Halaman</label>
                        <input type="text" class="form-control" id="jumlah_halaman" name="jumlah_halaman" value="{{ old('jumlah_halaman', '') }}" placeholder="Jumlah Halaman">
                        @error('jumlah_halaman')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="tahun_terbit">Tahun Terbit</label>
                        <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="{{ old('tahun_terbit', '') }}" placeholder="Masukan Tahun Terbit">
                        @error('tahun_terbit')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="{{ old('description', '') }}" placeholder="Masukan Description">
                        @error('description')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Buat Buku</button>
                    </div>
                </form>
                </div>
@endsection
