@extends('layouts.master')

@section('content')
    <div class="card">
              <div class="card-header">
                
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
              @endif
              
                <table class="table table-bordered">
                  <thead>
                    <tr>   
                      <th style="width: 20%">Judul : </th>
                      <th style="width: 80%">{{$buku->judul}}</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                
                    <tr>
                      <td>Description : </td>
                      <td>{{$buku->description}}</td>
                    </tr>
                    <tr>
                      <td>Tahun Terbit : </td>
                      <td>{{$buku->tahun_terbit}}</td>
                    </tr>
                    <tr>
                      <td>Jumlah Halaman : </td>
                      <td>{{$buku->jumlah_halaman}}</td>
                    </tr>
                
                  </tbody>
                </table>
                <a class="btn btn-info" href="/buku">Kembali</a>
              </div>
              <!-- /.card-body -->
              
            </div>
@endsection